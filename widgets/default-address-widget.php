<?php
class single_Service_Widget extends WP_Widget {
	public function __construct(  ) {
		$widget_options = [
			'classname'		=> 'default_address_widget',
			'description'	=> 'This Widget shows the default address set in Kyte Options'
		];

		parent::__construct( 'default_address_widget', 'Default Address Widget', $widget_options );
	}

	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance[ 'title' ] );
		//$blog_title = get_bloginfo( 'name' );
		//$tagline = get_bloginfo( 'description' );
		echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title'];


		$c = Timber::get_context(  );
		$fields = get_fields( 'options' );
		$c[ 'default_address' ] = $fields;
		$c[ 'social_media' ] = $fields[ 'social_media_grp' ];
		Timber::render( 'sidebars/widgets/default-address.twig', $c );

		echo $args['after_widget'];
	}

	/*public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : ''; ?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
		</p>
	<?php
	}*/

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
		return $instance;
	}
}

function keen_register_single_service_widget(  ) {
	register_widget( 'single_Service_Widget' );
} add_action( 'widgets_init', 'keen_register_single_service_widget' );


class footer_Navigation_Menus_Widget extends WP_Widget {
	public function __construct(  ) {
		$widget_options = [
			'classname'		=> 'footer_navigation_menus_widget',
			'description'	=> 'Not more than 5 widgets.'
		];

		parent::__construct( 'footer_navigation_menus_widget', 'Footer Navigation Menus Widget', $widget_options );
	}

	public function widget( $args, $instance ) {
		$fields = get_fields( 'widget_' . $args[ 'widget_id' ] );
		Timber::render(
			'sidebars/widgets/footer-navigation-menu-widget.twig',
			[
				'args'		=> $args,
				'instance'	=> $instance,
				'flds'		=> $fields
			]
		);
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : ''; ?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
		</p>
	<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
		return $instance;
	}
}

function kee_footer_navigation_menus_widget(  ) {
	register_widget( 'footer_Navigation_Menus_Widget' );
} add_action( 'widgets_init', 'kee_footer_navigation_menus_widget' );