<?php

$today = date( 'Ymd' );
$args =	[
			'post_type'			=> [ 'post' ],
			'post_status'		=> [ 'publish' ],
			'posts_per_page'	=> 2
		];

$c[ 'recent_posts' ] = new Timber\PostQuery( $args );