<?php

$today = date( 'Ymd' );
$args =	[
			'post_type'			=> [ 'single-case_study_cpt' ],
			'post_status'		=> [ 'publish' ],
			'posts_per_page'	=> 3
		];

$c[ 'recent_case_studies' ] = new Timber\PostQuery( $args );