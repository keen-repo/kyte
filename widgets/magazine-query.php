<?php

$args =	[
			'post_type'			=> [ 'magazine' ],
			'post_status'		=> [ 'publish' ],
			'posts_per_page'	=> 2,
			'offset'			=> 1
		];

$c[ 'magazines' ] = new Timber\PostQuery( $args );