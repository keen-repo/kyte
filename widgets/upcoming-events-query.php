<?php

$today = date( 'Y-m-d H:i:s' );
$args =	[
			'post_type'			=>	[ 'event' ],
			'post_status'		=>	[ 'publish' ],
			'posts_per_page'	=>	2,
			'meta_query'		=>	[
										[
											'key'		=> 'start_date',
											'compare'	=> '<=',
											'value'		=> $today
										],
										[
											'key'		=> 'end_date',
											'compare'	=> '>=',
											'value'		=> $today
										]
									]
		];

$c[ 'upcoming_events' ] = new Timber\PostQuery( $args );