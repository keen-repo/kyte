<?php
/**
 * Template Name: Events Archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$c = Timber::get_context(  );

$today = date( 'Y-m-d H:i:s' );
$args =	[
			'post_type'			=> [ 'event' ],
			'post_status'		=> [ 'publish' ],
			'paged'				=> $paged,
			'posts_per_page'	=> 4,
			'meta_query'		=> [
										[
											'key'		=> 'start_date',
											'compare'	=> '<=',
											'value'		=> $today
										],
										//[
										//	'key'		=> 'end_date',
										//	'compare'	=> '>=',
										//	'value'		=> $today
										//]
									]
		];

$c[ 'posts' ] = new Timber\PostQuery( $args );

$c[ 'post' ] = new TimberPost(  );

$c[ 'flds' ] = get_fields(  );

$c[ 'options' ] = get_fields( 'options' );

require get_template_directory() . '/widgets/recent-posts-query.php';

Timber::render( 'pages/archives/archive-event.twig', $c );
