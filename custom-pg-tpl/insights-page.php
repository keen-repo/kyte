<?php
/**
 * Template Name: Insights Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$args = [
	'post_type'		=>	'career',
	'paged'			=>	$paged,
];

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$args =	[
			'post_type'				=> [ 'post' ],
			'posts_per_page'		=> 1,
			'ignore_sticky_posts'	=> 1
		];

$c[ 'recent_posts' ] = new Timber\PostQuery( $args );

$args =	[
			'post_type'				=> [ 'news' ],
			'posts_per_page'		=> 1,
			'ignore_sticky_posts'	=> 1
		];

$c[ 'recent_news' ] = new Timber\PostQuery( $args );

$args =	[
			'post_type'				=> [ 'event' ],
			'posts_per_page'		=> 1,
			'ignore_sticky_posts'	=> 1
		];

$c[ 'events' ] = new Timber\PostQuery( $args );

$args =	[
			'post_type'				=> [ 'magazine' ],
			'posts_per_page'		=> 1,
			'ignore_sticky_posts'	=> 1
		];

$c[ 'magazine' ] = new Timber\PostQuery( $args );

$args =	[
			'post_type'				=> [ 'kyte_media' ],
			'posts_per_page'		=> 2,
			'ignore_sticky_posts'	=> 1
		];

$c[ 'media' ] = new Timber\PostQuery( $args );

$fields = get_fields( 'options' );

$c[ 'resource_links' ]		= $fields[ 'links_set' ];

/**
 * We're getting the resources page.
 * This is set in 'Custimisations' ( Options page ) where we usually set
 * the post type key, a name and the page it is archived on ( since archive pages must contain content other than the archive of its post type )
 */
$allResources = $fields[ 'post_types_rep' ];
foreach ( $allResources as $k => $l ) {
	if ( $l[ 'post_type_key' ] == 'resources' ) {
		$c[ 'all_resources' ] = $l[ 'post_type_archive_page' ];
	}
}

Timber::render( 'pages/templates/insights-page.twig', $c );
