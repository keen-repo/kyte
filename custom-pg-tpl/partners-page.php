<?php
/**
 * Template Name: Partners Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

Timber::render( 'pages/templates/partners-page.twig', $c );
