<?php
/**
 * Template Name: Services Archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$args = [
	'post_type'			=>	'service',
	'paged'				=>	$paged,
	'posts_per_page'	=>	16
];

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'the_top_text' ] = get_fields(  )[ 'top_text' ];

$c[ 'archive_post_types' ] = get_fields( 'options' )[ 'post_types_rep' ];

$c[ 'posts' ] = new Timber\PostQuery( $args );

Timber::render( 'pages/archives/archive-service.twig', $c );
