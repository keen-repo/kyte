<?php
/**
 * Template Name: Magazine Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$args = [
	'post_type'				=>	'magazine',
	'posts_per_page'		=> 3,
	//'offset'				=> 1
];

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'posts' ] = new Timber\PostQuery( $args );

$c[ 'options' ] = get_fields( 'options' );

require get_template_directory() . '/widgets/magazine-query.php';

Timber::render( 'pages/archives/archive-magazine.twig', $c );
