<?php
/**
 * Template Name: Careers Archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$args = [
	'post_type'		=>	'career',
	'paged'			=>	$paged,
];

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'the_top_text' ] = get_fields(  )[ 'top_text' ];

$c[ 'posts' ] = new Timber\PostQuery( $args );

Timber::render( 'pages/archives/archive-career.twig', $c );
