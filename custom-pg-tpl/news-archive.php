<?php
/**
 * Template Name: News Archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$args = [
	'post_type'		=>	'news',
	'paged'			=>	$paged,
];

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'posts' ] = new Timber\PostQuery( $args );

Timber::render( 'pages/archives/archive-news.twig', $c );
