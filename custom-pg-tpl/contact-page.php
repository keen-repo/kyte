<?php
/**
 * Template Name: Contact Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );
$c[ 'options' ] = get_fields( 'options' )[ 'contact_details' ];
$c[ 'other_pages' ] = get_fields( 'options' )[ 'other_pages_grp' ];

Timber::render( 'pages/templates/contact-page.twig', $c );
