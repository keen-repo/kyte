<?php
/**
 * Template Name: Media Archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$c = Timber::get_context(  );

$args = [
	'post_type'		=>	'kyte_media',
	'paged'			=>	$paged,
];

$c[ 'posts' ] = new Timber\PostQuery( $args );

require get_template_directory() . '/widgets/categories-query.php';

require get_template_directory() . '/widgets/upcoming-events-query.php';

$c[ 'post' ] = new TimberPost(  );

$c[ 'options' ] = get_fields( 'options' );


Timber::render( 'pages/archives/archive-kyte_media.twig', $c );
