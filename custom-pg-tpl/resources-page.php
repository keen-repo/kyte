<?php
/**
 * Template Name: Resources Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

$c[ 'flds' ] = get_fields(  );

$c[ 'options' ] = get_fields( 'options' );

$c[ 'post' ] = new TimberPost(  );

$postTypes = get_fields()[ 'posttypes' ];
$postTypeArr = [  ];

foreach ( $postTypes as $postType ) {
	$postTypeArr[  ] = $postType[ 'value' ];
}

$args1 = [
	'post_type'					=> $postTypeArr[ 0 ],
	'posts_per_page'			=> 1,
	'ignore_sticky_posts'		=> true
];

$args2 = [
	'post_type'					=> $postTypeArr[ 1 ],
	'posts_per_page'			=> 1,
	'ignore_sticky_posts'		=> true
];

$args3 = [
	'post_type'					=> $postTypeArr[ 2 ],
	'posts_per_page'			=> 1,
	'ignore_sticky_posts'		=> true
];

$c[ 'posts1' ] = new Timber\PostQuery( $args1 );
$c[ 'posts2' ] = new Timber\PostQuery( $args2 );
$c[ 'posts3' ] = new Timber\PostQuery( $args3 );

$c[ 'posttypes' ] = $postTypes;
require get_template_directory() . '/widgets/upcoming-events-query.php';

Timber::render( 'pages/templates/resources-page.twig', $c );
