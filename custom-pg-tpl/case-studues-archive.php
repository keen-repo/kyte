<?php
/**
 * Template Name: Case Studies Archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$args = [
	'post_type'			=>	'case_study_cpt',
	'paged'				=>	$paged,
	'posts_per_page'	=>	16
];

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'posts' ] = new Timber\PostQuery( $args );

$args =	[
			'post_type'			=> [ 'case_study_cpt' ],
			'posts_per_page'	=> 1
		];

$c[ 'posts' ] = new Timber\PostQuery( $args );

require get_template_directory() . '/widgets/recent-case-studies-query.php';

Timber::render( 'pages/archives/archive-case-studies.twig', $c );
