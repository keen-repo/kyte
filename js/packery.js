jQuery( document ).ready( function( $ ) {
	let	packeryFootersettings =	{
									itemSelector: 'section',
									gutter: '.gutter-sizer',
									percentPosition: true
								}

	//$( '.site-footer aside' ).packery( packeryFootersettings );

	let	widgetAreaonSmall =	{
								itemSelector: '.widget',
								gutter: 15,
							}

	$( window ).on( 'resize', function(  ) {
		if ( $( window ).outerWidth > 600 ) {
			$( '.custom-widget-area.d-lg-none' ).packery( widgetAreaonSmall );
		}
	} )
});