jQuery( document ).ready( function( $ ) {
	// init
	var controller = new ScrollMagic.Controller();

	var parallaxTL = new TimelineMax();
	parallaxTL
		.from( '.title_text_btn_bkimg_block .the-content', 0.4, { autoAlpha: 0, ease:Power0.easeNone }, 0.4 )
		.from( '.with-parallax', 2, { y: '-20%', ease:Power0.easeNone }, 0 );

	var slideParallaxScene = new ScrollMagic.Scene({
		triggerElement: '.has-parallax',
		triggerHook: 1,
		duration: '150%'
	})
		.setTween( parallaxTL )
		.addTo( controller );
/*
	var footerScene = new ScrollMagic.Scene( {
		triggerElement: '#footer-wrapper',
		duration: 1000,
		triggerHook: 1
	} )
		.setPin( '#footer-wrapper' )
		.addTo( controller2 );

	/*var newsy = new ScrollMagic.Scene( {
		triggerElement: '.temp-newsletter',
		duration: 1000,
		triggerHook: 1
	} )
		.setPin( '.temp-newsletter' )
		.addTo( controller2 );*/
} );