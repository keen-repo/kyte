jQuery( document ).ready( function( $ ) {
	let singleSlickSlidesSettings = {
		dots: true,
		arrows: true,
		nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
		prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
		infinite: true,
		speed: 700,
		autoplay: true,
		autoplaySpeed: 2500,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false
	};

	$('.hero-images').slick( singleSlickSlidesSettings );
	//$('.one-up-slider').slick( singleSlickSlidesSettings );

	// This one is for Testimonials
	let singleSlickTestimonialsSlidesSettings = {
		dots: false,
		arrows: true,
		nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
		prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
		infinite: true,
		speed: 700,
		autoplay: false,
		autoplaySpeed: 2000,
		slidesToShow: 1,
		slidesToScroll: 1
	};
	$('.one-up-slider').slick( singleSlickTestimonialsSlidesSettings );

	let threeUpImagesSlider = {
		dots: false,
		arrows: true,
		nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
		prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
		infinite: true,
		speed: 700,
		autoplay: true,
		autoplaySpeed: 6000,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	};

	$('.three-up-slider').slick( threeUpImagesSlider );

	let fourUpImagesSlider = {
		dots: false,
		arrows: true,
		nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
		prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
		infinite: true,
		speed: 700,
		autoplay: false,
		autoplaySpeed: 2000,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	};

	$('.four-up-slider').slick( fourUpImagesSlider );

	let datesSlider = {
		dots: false,
		arrows: true,
		nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
		prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
		infinite: false,
		speed: 700,
		autoplay: false,
		autoplaySpeed: 2000,
		slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth: true,
		//centerMode: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 600,
				settings: {
					variableWidth: true,
					centerMode: true,
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 300,
				settings: {
					variableWidth: true,
					centerMode: true,
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	};

	$('.dates-slider').slick( datesSlider );

	/*let postSliderImages = {
		centerMode: true,
		arrows: true,
		centerPadding: '0px',
		slidesToShow: 5,
		nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
		prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: true,
					centerMode: true,
					centerPadding: '0',
					slidesToShow: 3
				}
			},
			{
				breakpoint: 1,
				settings: {
					arrows: true,
					centerMode: false,
					centerPadding: '40px',
					slidesToShow: 1
				}
			}
		]
	};

	$('.post-slider-images').slick( postSliderImages );*/

	// Slide Counter
	let imagesSliderCounter = $( '.images-counter .counter' );
	$('.post-slider-images').on('afterChange', function( event, slick, currentSlide, nextSlide ) {
		imagesSliderCounter.text( currentSlide + 1 );
	});
} );