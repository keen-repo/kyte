//@prepros-append 'smooth-scroll.js'
//@prepros-append 'scrollMagicController.js'
//@prepros-append 'slick-slider-customisation.js'
//@prepros-append 'packery.js'

jQuery( document ).ready( function( $ ) {
	$( window ).on( 'load', onLoad );
	$( window ).on( 'resize', onResize );
	$( window ).on( 'scroll', onScroll );

	let top = $( window ).scrollTop(  );
	let siteHeader = $( '.site-header' );
	let siteHeaderHeight = siteHeader.outerHeight(  );
	let siteHeaderWrap = $( '.site-header-wrapper' );
	let siteContent = $( '.site-content' );
	let wW = $( window ).outerWidth(  );

	function onScroll(  ) {
		let delta = $( window ).scrollTop(  );
		if ( delta > top ) {
			siteHeaderWrap.removeClass( 'show' );
			siteHeader.removeClass( 'show white-background' );
		} else {
			siteHeaderWrap.addClass( 'show' );
			siteHeader.addClass( 'show white-background' );
			//siteContent.addClass( 'fade-me' );
		}
		top = delta;
		if ( delta == 0 ) {
			siteHeader.addClass( 'show' );
			siteHeader.removeClass( 'white-background' );
		}

		closeMenuOnScroll(  );
	}

	function onLoad(  ) {
		let siteHeader = $( '.site-header' );
		let siteHeaderHeight = siteHeader.outerHeight(  );
		let siteHeaderWrap = $( '.site-header-wrapper' );
		let siteContent = $( '.site-content' );
		let wW = $( window ).outerWidth(  );
		getHeaderHeight( siteHeader, siteHeaderHeight, siteHeaderWrap, siteContent, wW );

		$( '.preloader-wrapper' ).fadeOut(  );

		doMenu(  );
	}

	function onResize(  ) {
		setTimeout( function(  ) {
			let siteHeader = $( '.site-header' );
			let siteHeaderHeight = siteHeader.outerHeight(  );
			let siteHeaderWrap = $( '.site-header-wrapper' );
			let siteContent = $( '.site-content' );
			let wW = $( window ).outerWidth(  );
			getHeaderHeight( siteHeader, siteHeaderHeight, siteHeaderWrap, siteContent, wW );
		}, 250 );
	}

	function getHeaderHeight( siteHeader, siteHeaderHeight, siteHeaderWrap, siteContent, wW ) {
		let home = $( '.home' ).length;
		if ( !home ) {
			siteContent.css( 'margin-top', siteHeaderHeight );
		}
		if ( wW > 1023 ) {
		} else {
			siteContent.css( 'margin-top', '' );
		}
		//let wW = $( window ).outerWidth(  );
		let wH = $( window ).outerHeight(  );
		//let siteHeaderWrap = $( '.site-header' );
		let frontBanner = $( '.front-page-banner' );
		let siteHeaderWrapHeight = siteHeaderWrap.outerHeight(  );
		let bannerHeight = frontBanner.outerHeight(  );

		let marginHeight = bannerHeight - siteHeaderWrapHeight;

		front_Page_Banner_Height_Spacer( wW, marginHeight );
	}

	function front_Page_Banner_Height_Spacer( wW, marginHeight ) {
		if ( wW > 1024 ) {
			$( '.front-page-banner + div' ).css( 'margin-top', marginHeight ); // added '-60' to resolve an issue. This is not according to design anymore.
		} else {
			$( '.front-page-banner + div' ).css( 'margin-top', '' );
		}
	}

	$( '.custom-categories-widget .widget-title' ).on( 'click', dropHidden );
	$( '.single-service-widget .widget-title' ).on( 'click', dropHidden );

	let open = false;
	function dropHidden(  ) {
		let t = $( this );
		if ( open == false ) {
			$( '.custom-categories-widget ul .hide' ).addClass( 'show' );
			$( '.single-service-widget ul.hide' ).addClass( 'show' );
			open = true;
		} else {
			$( '.custom-categories-widget ul .hide' ).removeClass( 'show' );
			$( '.single-service-widget ul.hide' ).removeClass( 'show' );
			open = false;
		}
	}

	let allServices = $( '.view-all-services' );
	allServices.next().slideUp(  );
	let servicesOpen = false;
	allServices.on( 'click', function(  ) {
		if ( servicesOpen == false ) {
			$( this ).next(  ).slideDown(  );
			servicesOpen = true;
		} else {
			$( this ).next(  ).slideUp(  );
			servicesOpen = false;
		}
	} )

	/**
	 * Accordion setup
	 */
	let question = $( '.faqs .question' );
	let answers = $( '.faqs .answer' );
	question.on( 'click', openSibling );

	let setTitle = $( '.links-sets .link-set-title' );
	let setLinks = $( '.links-sets .link-set' );
	setTitle.on( 'click', openSibling );

	function openSibling(  ) {
		// if the current answer is open
		if ( $( this ).next().hasClass( 'open' ) ) {
			// close it up, add class 'hide' and remove class 'open'
			$( this ).next().slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
			$( this ).removeClass( 'current' );
		} else {
			// otherwise, close all answers up, add class 'hide' and remove class 'open'
			setLinks.slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
			answers.slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
			// then we open the answer to the question clicked
			$( this ).next( ).slideDown(  ).removeClass( 'hide' ).addClass( 'open' );
			question.removeClass( 'current' );
			$( this ).addClass( 'current' );
		}
	}

	let clickedCountry = $( '.countries .country' );
	clickedCountry.on( 'click', function(  ) {
		let t = $( this ).data( 'target' );
		$( '.country-wrap' ).removeClass( 'show' );
		$( '#country-' + t + '.country-wrap' ).addClass( 'show' );
	} );

	/**
	 * Footer Menu on small devices
	 */

	let widgetTitle = $( '#footer-wrapper .widget .widget-title' );
	let widgetMenus = $( '#footer-wrapper .widget .menu' );

	widgetTitle.one( 'click', widgetTitleClicked );
	function widgetTitleClicked( e ) {
		let wW = $( window ).outerWidth(  );
		console.log( wW )
		if ( wW < 1024 ) {
			e.preventDefault(  );
			widgetMenus.slideUp(  );
			$( this ).siblings( '.menu' ).slideDown(  );
			return false;
		}
	}

	AOS.init({
		easing: 'ease-out-back',
		duration: 1000,
		startEvent: 'load'
	});

	function doMenu(  ) {

		$( '.site-header-wrapper .menu-toggle, .site-header-wrapper .close-menu, .menu-underlay' ).on( 'click', openCloseMenu );

		function openCloseMenu(  ) {
			let $mainMenu = $( '.main-navigation' );
			let $menuUnderlay = $( '.menu-underlay' );
			if ( $mainMenu.hasClass( 'open' ) ) {
				$mainMenu.removeClass( 'open' );
				$menuUnderlay.removeClass( 'open' );
			} else {
				$mainMenu.addClass( 'open' );
				$menuUnderlay.addClass( 'open' );
			}
		}

		$( '.main-navigation li.menu-item-has-children > a' ).after( '<span class="triangle"></span>' );

		$( '.main-navigation li.menu-item-has-children .triangle' ).on( 'click', function( e ) {
		  e.preventDefault(  );

		  let $subMenu = $( this ).next( '.sub-menu' );
		  let $opened = $( '.sub-menu.open' );

		  if ( $( this ).hasClass( 'open' ) ) {
		    $( this ).removeClass( 'open' );
		    $subMenu.removeClass( 'open' ).slideUp(  );
		  } else {
		    $opened.slideUp(  );
		    $( '.mobile-menu .triangle' ).removeClass( 'open' );
		    $( this ).addClass( 'open' );
		    $subMenu.addClass( 'open' ).slideDown(  );
		  }
		} );
	}

	function closeMenuOnScroll(  ) {
		let $mainMenu = $( '.main-navigation' );
		let $menuUnderlay = $( '.menu-underlay' );
		$menuUnderlay.removeClass( 'open' );
		$mainMenu.removeClass( 'open' );
	}

	$( '.frm_checkbox' ).before( `
		<p class="keen-fields-marked">All fields marked with an * are required</p>
	` );
	function silentErrorHandler() {return true;}
	window.onerror=silentErrorHandler;
} );