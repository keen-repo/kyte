<form role="search" method="get" id="searchform" class="search-form" action="/" >
	<div>
		<label class="screen-reader-text" for="s"><?php __( 'Search for:' ); ?></label>
		<input class="search-field" type="text" value="<?php get_search_query(); ?>" name="s" id="s" placeholder="<?php _e( 'SEARCH', 'kyte' ); ?>" />
		<img class="magnifier" src="<?= get_template_directory_uri(  ) . '/assets/images/magnifier.png' ?>" alt="">
		<input class="search-submit" type="submit" id="searchsubmit" value="<?php echo esc_attr__( 'Search', 'kyte' ); ?>" />
	</div>
</form>