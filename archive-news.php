<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

//get_header();

$fields = get_fields( 'options' )[ 'post_types_rep' ];

$postType = get_post_type(  );

foreach ( $fields as $k => $f ) {
	$key = $f[ 'post_type_key' ];
	$name = $f[ 'post_type_name' ];
	$page = $f[ 'post_type_archive_page' ];
	$image = $f[ 'archive_page_image' ];

	if ( $key == $postType ) {
		wp_redirect( $f[ 'post_type_archive_page' ] );
	}
}


$c = Timber::get_context(  );

$c[ 'posts' ] = new Timber\PostQuery(  );

$c[ 'posttype' ] = get_post_type(  );

$c[ 'post_types_rep' ] = get_fields( 'options' )[ 'post_types_rep' ];

Timber::render( 'pages/archives/archive-service.twig', $c );