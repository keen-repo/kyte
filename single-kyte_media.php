<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Kyte_Solutions
 */

get_header();

//load_theme_textdomain( 'kyte', get_template_directory() . '/languages' );

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'post_types_rep' ] = get_fields( 'options' )[ 'post_types_rep' ];
$c[ 'posttype' ] = get_post_type(  );

$c[ 'options' ] = get_fields( 'options' )[ 'formidable_forms_ids' ];

//$c[ 'sidebar' ] = Timber::get_sidebar( 'sidebar.php' );

$args = [
	'post_type'			=> 'service',
];

$c[ 'side_posts' ] = new Timber\PostQuery( $args );

Timber::render( 'pages/singles/single-kyte_media.twig', $c );
