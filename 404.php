<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

Timber::render( 'pages/singles/404.twig', $c );