<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kyte_Solutions
 */

$c = Timber::get_context(  );

$c[ 'sidebar' ] = Timber::get_sidebar( 'sidebar.php' );

$fields = get_fields( 'options' );

$c[ 'default_address' ] = $fields;

$c[ 'footer_widgets' ] = Timber::get_widgets( 'footer-1' );

$c[ 'other_pages' ] = $fields[ 'other_pages_grp' ];

Timber::render( 'sections/footer/footer.twig', $c );