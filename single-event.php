<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'post_types_rep' ] = get_fields( 'options' )[ 'post_types_rep' ];
$c[ 'posttype' ] = get_post_type(  );

$c[ 'options' ] = get_fields( 'options' )[ 'formidable_forms_ids' ];

require_once 'widgets/upcoming-events-query.php';

Timber::render( 'pages/singles/single-event.twig', $c );