<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'post_types_rep' ] = get_fields( 'options' )[ 'post_types_rep' ];
$c[ 'posttype' ] = get_post_type(  );

$c[ 'options' ] = get_fields( 'options' )[ 'formidable_forms_ids' ];
$others = get_field( 'select_other_services_solutions' );
$othersArr = [  ];
$keysArr = [ 'post_id', 'post_icon', 'post_link' ];
$keysIndex = 0;

if ( $others ) {
	foreach ( $others as $other ) {
		$postID		= $other->ID;
		$postIcon	= get_field( 'icon', $other->ID );
		$postTitle	= $other->post_title;
		$postLink 	= get_the_permalink( $other->ID );
		$othersArr[] = [
			'post_id'		=> $postID,
			'post_icon'		=> $postIcon,
			'post_title'	=> $postTitle,
			'post_link'		=> $postLink
		];
	}

	$c[ 'others' ] = $othersArr;
}
/*
$args = [
	'post_type'			=> 'service',
	'posts_per_page'	=> -1
];

$c[ 'side_posts' ] = new Timber\PostQuery( $args );

wp_reset_postdata();
wp_reset_query();
*/
Timber::render( 'pages/singles/single-pci.twig', $c );
