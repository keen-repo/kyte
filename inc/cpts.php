<?php

add_action( 'init', 'register_cpts', 10, 0 );
function register_cpts(  ) {
	$settings =	[
					# Add the post type to the site's main RSS feed:
					'show_in_feed'		=>	true,
					'show_in_menu'		=> 'cpts_menu', # Show under Kyte custom admin menu
					'capability_type'	=>	'post',
					'supports'			=>	[
												'title',
												'editor',
												'author',
												'thumbnail',
												'revisions',
												'page-attributes',
												'post-formats',
												'excerpt'
											],
					'show_in_rest'		=>	true,
					'hierarchical'		=>	false
				];
	# $names, change for each post type
	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'Service',
					'plural'			=> 'Services',
					'slug'				=> 'service',
				];

	# register cornucopia post type
	register_extended_post_type( 'service', $settings, $names );

	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'Solution',
					'plural'			=> 'Solutions',
					'slug'				=> 'solution',
				];

	register_extended_post_type( 'solutions_cpt', $settings, $names );

	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'Case Study',
					'plural'			=> 'Case Studies',
					'slug'				=> 'case-study',
				];

	register_extended_post_type( 'case_study_cpt', $settings, $names );

	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'Media',
					'plural'			=> 'Media',
					'slug'				=> 'media',
				];

	# register cornucopia post type
	register_extended_post_type( 'kyte_media', $settings, $names );

	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'News',
					'plural'			=> 'News',
					'slug'				=> 'news',
				];

	register_extended_post_type( 'news', $settings, $names );

	// MAGAZINE POST TYPE
	$settings =	[
					# Add the post type to the site's main RSS feed:
					'show_in_feed'		=>	true,
					'show_in_menu'		=> 'cpts_menu', # Show under Kyte custom admin menu
					'capability_type'	=>	'post',
					'supports'			=>	[
												'title',
											],
					'show_in_rest'		=>	true,
					'hierarchical'		=>	false,
					'admin_cols'		=> 	[
												'issue_date'		=>	[
																			'title'			=> 'Issue Date',
																			'meta_key'		=> 'issue_date',
																			'date_format'	=> 'dS M, Y'
																		],
												'published_date'	=>	[
																			'title'			=> 'Post published on',
																			'post_field'	=> 'post_date',
																			'date_format'	=> 'dS M, Y'
																		]
											]
				];

	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'Magazine',
					'plural'			=> 'Magazines',
					//'slug'				=> 'news',
				];

	register_extended_post_type( 'magazine', $settings, $names );
	// MAGAZINE POST TYPE - END


	// PCI POST TYPE
	$settings =	[
		# Add the post type to the site's main RSS feed:
		'show_in_feed'		=>	true,
		'show_in_menu'		=> 'cpts_menu', # Show under Kyte custom admin menu
		'capability_type'	=>	'post',
		'supports'			=>	[
									'title',
									'thumbnail',
								],
		'show_in_rest'		=>	true,
		'hierarchical'		=>	false,
		'admin_cols'		=> 	[
									'issue_date'		=>	[
																'title'			=> 'Issue Date',
																'meta_key'		=> 'issue_date',
																'date_format'	=> 'dS M, Y'
															],
									'published_date'	=>	[
																'title'			=> 'Post published on',
																'post_field'	=> 'post_date',
																'date_format'	=> 'dS M, Y'
															]
								]
	];

$names =	[
		# Override the base names used for labels:
		'singular'			=> 'PCI',
		'plural'			=> 'PCIs',
		//'slug'				=> 'news',
	];

register_extended_post_type( 'pci', $settings, $names );
// PCI POST TYPE - END


	$settings =	[
					# Add the post type to the site's main RSS feed:
					'show_in_feed'		=>	true,
					'show_in_menu'		=> 'cpts_menu', # Show under Kyte custom admin menu
					'capability_type'	=>	'post',
					'supports'			=>	[
												'title',
												'editor',
												'thumbnail',
												'revisions',
												'page-attributes',
												'post-formats',
												'excerpt'
											],
					'show_in_rest'		=>	true,
					'hierarchical'		=>	false,
					'admin_cols'		=> 	[
												'start_date'	=>	[
																		'title'			=> 'Start Date',
																		'meta_key'		=> 'start_date',
																		'date_format'	=> 'dS M, Y - H:i'
																	],
												'end_date'	=>	[
																		'title'			=> 'End Date',
																		'meta_key'		=> 'end_date',
																		'date_format'	=> 'dS M, Y - H:i'
																	]
											]
				];

	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'Event',
					'plural'			=> 'Events',
					'slug'				=> 'events',
				];

	register_extended_post_type( 'event', $settings, $names );


	$settings =	[
					# Add the post type to the site's main RSS feed:
					'show_in_feed'		=>	true,
					'show_in_menu'		=> 'cpts_menu', # Show under Kyte custom admin menu
					'capability_type'	=>	'post',
					'supports'			=>	[
												'title',
												'thumbnail'
											],
					'show_in_rest'		=>	false,
					'hierarchical'		=>	false
				];

	$names =	[
					# Override the base names used for labels:
					'singular'			=> 'Career',
					'plural'			=> 'Careers',
					//'slug'				=> 'career',
				];

	register_extended_post_type( 'career', $settings, $names );
}


/**
 * Register Hotels custom menu page.
 */
function vjborg_hotels_menu(){
	add_menu_page(
		'Kyte',
		'Kyte',
		'edit_posts',
		'cpts_menu',
		'',
		'dashicons-admin-home',
		2
	);
} add_action( 'admin_menu', 'vjborg_hotels_menu' );