<?php

// Back End Styles
function custom_gutenberg_styles() {
	wp_enqueue_style('custom-gutenberg', get_theme_file_uri( '/css/acf-blocks.css' ), false, '1.0', 'all');
	wp_enqueue_style('helper-images', get_theme_file_uri( '/css/admin-helper-images.css' ), false, '1.0', 'all');
}
add_action('enqueue_block_editor_assets', 'custom_gutenberg_styles');

add_action( 'enqueue_block_assets', 'keen_acf_blocks_scripts', 10, 0 );
function keen_acf_blocks_scripts() {
	$tplDir		= get_template_directory_uri();

	// Custom Blocks CSS
	$link = "{$tplDir}/css/acf-blocks.css";
	wp_enqueue_style( 'keen-acf-blocks-css', $link, array(), "0.0.1", 'all' );

	// Bulma
	$link = "{$tplDir}/installs/bulma_075/css/bulma.min.css";
//	wp_enqueue_style( 'keen-bulma-css', $link, false, '0.7.5' );
}


function vjborg_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		[
			[
				'slug' => 'kyte_block_category',
				'title' => __( 'Kyte Solutions Blocks', 'kyte_block' ),
			],
		]
	);
} add_filter( 'block_categories', 'vjborg_block_category', 10, 2);



add_action('acf/init', 'my_register_blocks');
function my_register_blocks() {

	// check function exists.
	if ( function_exists( 'acf_register_block_type' ) ) {

		$settings = [
						'name'              => 'display_post_type',
						'title'             => __('Display A Post Type'),
						'description'       => __('Display a number of post types'),
						'render_callback'   => 'display_post_type',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'title_text_btn_bkimg_block',
						'title'             => __('Title + Text + Btn w/ Background Image'),
						'description'       => __('Title + Text + Btn w/ Background Image'),
						'render_callback'   => 'title_text_btn_bkimg_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'testimonials_block',
						'title'             => __('Testimonials Block'),
						'description'       => __('Testimonials Block'),
						'render_callback'   => 'testimonials_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'slider_images_block',
						'title'             => __('Slider Images Block'),
						'description'       => __('Slider Images Block'),
						'render_callback'   => 'slider_images_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'insights_3_archives_block',
						'title'             => __('Insights 3 Archives Block'),
						'description'       => __('Insights 3 Archives Block'),
						'render_callback'   => 'insights_3_archives_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'timeline_block',
						'title'             => __('Timeline Block'),
						'description'       => __('Timeline Block'),
						'render_callback'   => 'timeline_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'logo_properties_block',
						'title'             => __('Logo w/ 4 Properties Block'),
						'description'       => __('Logo w/ 4 Properties Block'),
						'render_callback'   => 'logo_properties_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'map_points_block',
						'title'             => __('Map Points Block'),
						'description'       => __('Map Points Block'),
						'render_callback'   => 'map_points_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'team_block',
						'title'             => __('Team Members Block'),
						'description'       => __('Team Members Block'),
						'render_callback'   => 'team_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'list_block',
						'title'             => __('Lists Block'),
						'description'       => __('Lists Block'),
						'render_callback'   => 'list_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'logo_in_square_block',
						'title'             => __('Logos in Square Block'),
						'description'       => __('Logos in Square Block'),
						'render_callback'   => 'logo_in_square_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'title_text_btn_block',
						'title'             => __('Title, Text and Button Block'),
						'description'       => __('Title, Text and Button Block'),
						'render_callback'   => 'title_text_btn_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'kyte_faq_block',
						'title'             => __('FAQs Block'),
						'description'       => __('FAQs Block'),
						'render_callback'   => 'kyte_faq_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'resource_links_block',
						'title'             => __('Resource Links Block'),
						'description'       => __('Resource Links Block'),
						'render_callback'   => 'resource_links_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'color_company_logos_block',
						'title'             => __('Colour Company Logos Block'),
						'description'       => __('Colour Company Logos Block'),
						'render_callback'   => 'color_company_logos_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'become_a_partner_block',
						'title'             => __('Become a partner Form'),
						'description'       => __('Become a partner Form'),
						'render_callback'   => 'become_a_partner_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'personal_details_block',
						'title'             => __('Personal Details Form'),
						'description'       => __('Personal Details Form'),
						'render_callback'   => 'personal_details_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'ebook_download_block',
						'title'             => __('eBook Download Form'),
						'description'       => __('eBook Download Form'),
						'render_callback'   => 'ebook_download_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );

		$settings = [
						'name'              => 'text_search_block',
						'title'             => __('Text and Search Form'),
						'description'       => __('Text and Search Form'),
						'render_callback'   => 'text_search_block',
						'category'          => 'kyte_block_category',
						'html'              => true,
						'align'             => 'full',
						'supports'          =>  [
													'align' => false,
												],
					];
		acf_register_block_type( $settings );
	}
}

/**
 * Testimonial Block Callback Function.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

function display_post_type( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'display_post_type_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$display_post_type = get_field( 'posttype' );

	$args = [
		'post_type'			=> [ $display_post_type ],
		'posts_per_page'	=> 12
	];
	$c[ 'archive_post_types' ] = get_fields( 'options' )[ 'post_types_rep' ];
	$c[ 'posts' ] = new Timber\PostQuery( $args );
	$c[ 'posttype' ] = $display_post_type;
	$c[ 'flds' ]	= get_fields();

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/display_post_type.twig', $c );
	}
}

function title_text_btn_bkimg_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'title_text_btn_bkimg_block has-parallax';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/title_text_btn_bkimg_block.twig', $c );
	}
}

function testimonials_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'testimonials_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();
	$c[ 'options' ]	= get_fields( 'options' )[ 'testimonials_grp' ];

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/testimonials_block.twig', $c );
	}
}

function slider_images_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'slider_images_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/slider_images_block.twig', $c );
	}
}

function insights_3_archives_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'insights_3_archives_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	//$c[ 'flds' ]	= get_fields();
	$postTypes = get_fields()[ 'posttypes' ];
	$postTypeArr = [  ];

	foreach ( $postTypes as $postType ) {
		$postTypeArr[  ] = $postType[ 'value' ];
	}

	$args1 = [
		'post_type'					=> $postTypeArr[ 0 ],
		'posts_per_page'			=> 1,
		'ignore_sticky_posts'		=> true
	];

	$args2 = [
		'post_type'					=> $postTypeArr[ 1 ],
		'posts_per_page'			=> 1,
		'ignore_sticky_posts'		=> true
	];

	$args3 = [
		'post_type'					=> $postTypeArr[ 2 ],
		'posts_per_page'			=> 1,
		'ignore_sticky_posts'		=> true
	];

	$c[ 'posts1' ] = new Timber\PostQuery( $args1 );
	$c[ 'posts2' ] = new Timber\PostQuery( $args2 );
	$c[ 'posts3' ] = new Timber\PostQuery( $args3 );

	$c[ 'posttypes' ] = $postTypes;
	$c[ 'flds' ] = get_fields(  );

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/insights_3_archives_block.twig', $c );
	}
}

function timeline_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'timeline_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();
	$c[ 'options' ] = get_fields( 'options' )[ 'timeline' ];

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/timeline_block.twig', $c );
	}
}

function logo_properties_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'logo_properties_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/logo_properties_block.twig', $c );
	}
}

function map_points_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'map_points_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	  = get_fields();
	$c[ 'points' ]    = get_fields( 'options' )[ 'map_coordinates' ];

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/map_points_block.twig', $c );
	}
}

function team_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'team_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();
	$c[ 'primary_members' ]	= get_fields( 'options' )[ 'primary_members' ];
	$c[ 'other_team_members' ]	= get_fields( 'options' )[ 'other_team_members' ];

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/team_block.twig', $c );
	}
}

function list_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'list_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();
	$c[ 'primary_members' ]	= get_fields( 'options' )[ 'primary_members' ];
	$c[ 'other_team_members' ]	= get_fields( 'options' )[ 'other_team_members' ];

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/list_block.twig', $c );
	}
}

function logo_in_square_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'logo_in_square_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$fields			= get_fields();
	$options		= get_fields( 'options' );

	$c[ 'flds' ]	= $fields;
	$c[ 'options' ]	= $options[ 'other_pages_grp' ];

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/logo_in_square_block.twig', $c );
	}
}

function title_text_btn_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'title_text_btn_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/title_text_btn_block.twig', $c );
	}
}

function kyte_faq_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'kyte_faq_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]	= get_fields();

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/kyte_faq_block.twig', $c );
	}
}

function resource_links_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'resource_links_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]    = get_fields();
	$c[ 'options' ]    = get_fields( 'options' )[ 'links_set' ];

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/resource_links_block.twig', $c );
	}
}

function color_company_logos_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'color_company_logos_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ]    = get_fields();

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/color_company_logos_block.twig', $c );
	}
}

function become_a_partner_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'become_a_partner_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ] = get_fields(  );

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/become_a_partner_block.twig', $c );
	}
}

function personal_details_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'personal_details_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ] = get_fields(  );

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/personal_details_block.twig', $c );
	}
}

function ebook_download_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'ebook_download_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$formID = get_field( 'form_id' );

	$c[ 'flds' ] = get_fields(  );
	$c[ 'post' ] = new TimberPost(  );

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/ebook_download_block.twig', $c );
	}
}

function text_search_block( $block, $content = '', $is_preview = false, $post_id = 0 ) {

	$c = Timber::get_context();

	// Create id attribute allowing for custom "anchor" value.
	$c[ 'id' ] = $block[ 'id' ];
	if ( !empty( $block[ 'anchor' ] ) ) {
		$c[ 'id' ] = $block[ 'anchor' ];
	}

	// Create class attribute allowing for custom "class_name" and "align" values.
	$c[ 'class_name' ] = 'text_search_block';
	if ( !empty( $block[ 'class_name' ] ) ) {
		$c[ 'class_name' ] .= ' ' . $block[ 'class_name' ];
	}
	if ( !empty( $block[ 'align' ] ) ) {
		$c[ 'class_name' ] .= ' align' . $block[ 'align' ];
	}

	$c[ 'flds' ] = get_fields(  );
	$c[ 'post' ] = new TimberPost(  );

	$active = $c[ 'flds' ][ 'sec_prop' ][ 'active' ];
	if ( $active ) {
		Timber::render( 'acf-blocks/text_search_block.twig', $c );
	}
}
