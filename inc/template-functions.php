<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Kyte_Solutions
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function kyte_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
//add_filter( 'body_class', 'kyte_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function kyte_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'kyte_pingback_header' );

function acf_load_select_post_types_field_choices( $field ) {
	// reset choices
	$field[ 'choices' ] = [  ];
	// get the textarea value from options page without any formatting
	if ( have_rows( 'post_types_rep', 'option' ) ) {
		while ( have_rows( 'post_types_rep', 'option' ) ) { the_row(  );
			$value = get_sub_field( 'post_type_key' );
			$label = get_sub_field( 'post_type_name' );

			$field[ 'choices' ][ $value ] = $label;
		}
	}
	// return the field
	return $field;
} add_filter( 'acf/load_field/name=select_post_types', 'acf_load_select_post_types_field_choices' );

function frm_set_checked_one( $values, $field ) {
	global $post;
	// get current post title
	$postTitle = $post->title;

	//echo $postTitle;
	//echo $post->ID;

	//$console = json_encode( $field );
	//echo '<script>console.log(' . $console . ')</script>';

	$args =	[
				'post_type' 		=> 'service',
				'orderby'			=> 'title',
				'order'				=> 'ASC',
				'suppress_filters'	=> false
			];
	$services = get_posts( $args );

	$arr = [ $postTitle ];

	foreach ( $services as $service ) {
		$serviceTitle = $service->post_title;

		if ( $postTitle != $serviceTitle ) {
			array_push( $arr, $service->post_title );
		}
	}

	switch ( ICL_LANGUAGE_CODE ) {
		case 'en':
			$fieldKey = 'pd5r6';
			break;

		case 'it':
			$fieldKey = 'services_it';
			break;

		default:
			# code...
			break;
	}


	if ( $field->field_key == $fieldKey ) {//Replace 125 with the ID of your field
		$values[ 'options' ] = $arr; //Replace Option1 and Option2 with the options you want in the field
	}
	return $values;
} add_filter( 'frm_setup_new_fields_vars', 'frm_set_checked_one', 20, 2 );

function change_my_confirmation_method( $type, $form ) {
	global $post;
	$file = get_field( 'download_file', $post->ID );
	$btn = $form->options[ 'submit_value' ];

	$formID = get_field( 'form_id' );

	if ( $form->id == $formID ) { //change 5 to the ID of your form ?>
		<div class="btn-wrapper">
			<a href="<?= $file; ?>" download target="_blank" class="btn primary-btn" data-aos="fade" data-aos-delay="0" data-aos-offset="-50"><?= $btn; ?></a>
		</div>
	<?php
	}
} add_filter('frm_success_filter', 'change_my_confirmation_method', 10, 2);


function searchfilter( $query ) {
	if ( $query->is_search && !is_admin() ) {
		$query->set(
			'post_type',
			[
				'service',
				'solutions_cpt',
				'kyte_media',
				'news',
				'magazine',
				'event',
			 	'post'
			]
		);
	}
	return $query;
} add_filter( 'pre_get_posts','searchfilter' );

function kyte_custom_login_url($url) {
	return get_home_url( );
} add_filter( 'login_headerurl', 'kyte_custom_login_url' );