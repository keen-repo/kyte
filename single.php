<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Kyte_Solutions
 */

get_header();

// Timber support for Multipage
global $post, $page, $pages, $multipage;
setup_postdata( $post );

$c = Timber::get_context();
$c['post'] = new TimberPost();

// Overwrite whole post with paginated piece, if viewing a
// paginated page of the overall post
if ( $multipage ) {
	$c['post']->post_content = $pages[ $page - 1 ];
}

if ( post_password_required() ) {
	$c['password'] = true;
}

$args =	[
			'echo'		=> 0,
			'style'		=> 'l'
		];

$c[ 'categories' ] = wp_list_categories( $args );

$args = [
	'post_type'				=> 'post',
	'posts_per_page'		=> 2,
	'ignore_sticky_posts'	=> 1
];

$c[ 'recent_posts' ] = new Timber\PostQuery( $args );





$postTypes =	[
					[ 'value' => 'news', 'label' => __( 'news', 'kyte' ) ],
					[ 'value' => 'event', 'label' => __( 'events', 'kyte' ) ],
					[ 'value' => 'post', 'label' => __( 'blog', 'kyte' ) ]
				];

$postTypeArr = [  ];

foreach ( $postTypes as $postType ) {
	$postTypeArr[  ] = $postType[ 'value' ];
}

$args1 = [
	'post_type'					=> $postTypeArr[ 0 ],
	'posts_per_page'			=> 1,
	'ignore_sticky_posts'		=> true
];

$args2 = [
	'post_type'					=> $postTypeArr[ 1 ],
	'posts_per_page'			=> 1,
	'ignore_sticky_posts'		=> true
];

$args3 = [
	'post_type'					=> $postTypeArr[ 2 ],
	'posts_per_page'			=> 1,
	'ignore_sticky_posts'		=> true,
	'post__not_in' => array( $post->ID )
];

$c[ 'posts1' ] = new Timber\PostQuery( $args1 );
$c[ 'posts2' ] = new Timber\PostQuery( $args2 );
$c[ 'posts3' ] = new Timber\PostQuery( $args3 );

$c[ 'posttypes' ] = $postTypes;
$c[ 'flds' ] = get_fields(  );











Timber::render( 'pages/singles/single.twig', $c );
