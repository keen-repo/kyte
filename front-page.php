<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

$c[ 'flds' ] = get_fields(  );

$c[ 'post' ] = new TimberPost(  );

// Get the Our Services section
$args = [
	'post_type'			=> 'service',
	'posts_per_page'	=> 12
];

$c[ 'services' ] = new Timber\PostQuery( $args );


Timber::render( 'pages/front-page.twig', $c );

