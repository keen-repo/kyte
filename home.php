<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kyte_Solutions
 */

get_header();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

$c = Timber::get_context(  );

$args =	[
			'post_type'			=> [ 'post' ],
			'post_status'		=> [ 'publish' ],
			'paged'				=> $paged,
			'posts_per_page'	=> 3
		];

$c[ 'posts' ] = new Timber\PostQuery( $args );

require get_template_directory() . '/widgets/categories-query.php';

require get_template_directory() . '/widgets/upcoming-events-query.php';

$c[ 'post' ] = new TimberPost(  );

Timber::render( 'pages/home.twig', $c );
