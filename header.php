<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kyte_Solutions
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163713379-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-163713379-1');
	</script>

	<?php wp_head(); ?>
</head>
<?php
require_once get_template_directory() . '/preloader.html';
?>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'kyte' ); ?></a>

	<header id="masthead" class="site-header show">
		<div class="site-header-wrapper">
			<div class="site-branding">
				<?php
				the_custom_logo();
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif;
				$kyte_description = get_bloginfo( 'description', 'display' );
				if ( $kyte_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $kyte_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fas fa-bars"></i></button>
			<nav id="site-navigation" class="main-navigation">
				<button class="close-menu"><i class="fas fa-times"></i></button>
				<?php
				/*
				*/
				?>
				<?php
				wp_nav_menu( array(
					'theme_location' 	=> 'menu-1',
					'menu_id'        	=> 'primary-menu',
					'container_class'	=> 'main-menu-wrapper',
					'depth'				=> 2
				) );

				wp_nav_menu( array(
					'theme_location' 	=> 'menu-1',
					'menu_id'        	=> 'mobile-primary-menu',
					'container_class'	=> 'mobile-menu-wrapper',
					'depth'				=> 2
				) );

				/**
				 * 'socialang-1' menu contains only the WPML language switcher so we get it as Timber\Menu
				 * Then we get the social media from the options pages and combine the two together
				 * @var [type]
				 */
				$c = Timber::get_context(  );
				$c[ 'social_media' ] = get_fields( 'options' )[ 'social_media_grp' ];
				$c[ 'socialang' ] = new Timber\Menu( 'socialang-1' );
				Timber::render( 'menus/social-language-menu.twig', $c );
				?>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
