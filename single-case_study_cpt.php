<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Kyte_Solutions
 */

get_header();

$c = Timber::get_context(  );

$c[ 'post' ] = new TimberPost(  );

$c[ 'post_types_rep' ] = get_fields( 'options' )[ 'post_types_rep' ];
$c[ 'posttype' ] = get_post_type(  );

$fields = get_fields(  );
$options = get_fields( 'options' );

$c[ 'flds' ] = $fields;
$c[ 'ff_forms' ] = $options[ 'formidable_forms_ids' ];

require get_template_directory() . '/widgets/recent-case-studies-query.php';

Timber::render( 'pages/singles/single-case_study_cpt.twig', $c );